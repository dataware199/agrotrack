from django.db import models
from django.utils.timezone import now

# Create your models here.
class Setting_region(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateField(default=now)
    name_region = models.CharField(max_length=200) 
    
    class Meta:
            ordering: ['-id']

