from django.shortcuts import render, redirect
from .models import Setting_region
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.paginator import Paginator
import json
from django.http import JsonResponse
import datetime
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse




# Create your views here.
def index(request):
    
    setting_region = Setting_region.objects.all()
    paginator = Paginator(setting_region, 5)
    page_number = request.GET.get('page')
    page_obj = Paginator.get_page(paginator, page_number)
  
    context = {
        'setting_region': setting_region,
        'page_obj': page_obj
       
    }
    return render(request, 'setting_region/index.html', context)


