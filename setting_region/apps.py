from django.apps import AppConfig


class SettingRegionConfig(AppConfig):
    name = 'setting_region'
