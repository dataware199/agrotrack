"""agrotrack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('setting_crop.urls')),
    path('authentication/', include('authentication.urls')),
#    ('apply_loan/', include('apply_loan_tbl.urls')),
#   path('market_data/', include('setting_market_data.urls')),
    path('farmer/', include('farmer_tbl.urls')),
    path('district/', include('setting_district.urls')),
    path('region/', include('setting_region.urls')),
    path('admin/', admin.site.urls)
]
