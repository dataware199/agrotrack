from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Farmer_tbl 

# Create your views here.
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.paginator import Paginator
import json
from django.http import JsonResponse

import datetime




def index(request):
    farmer = Farmer_tbl.objects.all()
    paginator = Paginator(farmer, 5)
    page_number = request.GET.get('page')
    page_obj = Paginator.get_page(paginator, page_number)
  
    context = {
        'farmer': farmer,
        'page_obj': page_obj
       
    }
    return render(request, 'farmer/index.html', context)

def add_farmer(request):
  
    context = {
      
        'values': request.POST
    }
    if request.method == 'GET':
        return render(request, 'farmer/add_farmer.html', context)

    if request.method == 'POST':
        first_name = request.POST['first_name']

        if not first_name:
            messages.error(request, 'First Name is required')
            return render(request, 'farmer/add_farmer.html', context)
        middle_name = request.POST['middle_name']
        last_name = request.POST['last_name']
        date_birth = request.POST['date_birth']
        phone_number = request.POST['phone_number'] 
        identification = request.POST['identification']
        age = request.POST['age']
       

        if not last_name:
            messages.error(request, 'Last Name is required')
            return render(request, 'farmer/add_farmer.html', context)

        Farmer_tbl.objects.create(  middle_name=middle_name, age=age, phone_number=phone_number, last_name=last_name, first_name=first_name, identification=identification
                                )
        messages.success(request, 'Expense saved successfully')

        return redirect('farmer')
    

def farmer_edit(request, id):
    farmer = Farmer_tbl.objects.get(pk=id)
   
    context = {
        'farmer': farmer,
        'values': farmer,
       
    }
    if request.method == 'GET':
        return render(request, 'farmer/edit-farmer.html', context)
    if request.method == 'POST':
        phone_number = request.POST['phone_number']

        if not phone_number:
            messages.error(request, 'Phone is required')
            return render(request, 'farmer/edit-expense.html', context)
        first_name = request.POST['first_name']
        middle_name = request.POST['middle_name']
        last_name = request.POST['last_name']
        date_birth = request.POST['date_birth']
        phone_number = request.POST['phone_number'] 
        identification = request.POST['identification']
        age = request.POST['age']
       

        if not age:
            messages.error(request, 'Age is required')
            return render(request, 'age/edit-expense.html', context)

       
        farmer.first_name = first_name
        farmer.middle_name = middle_name 
        farmer.last_name = last_name
        farmer.date_birth = date_birth
        farmer.phone_number = phone_number
        farmer.identification = identification

        farmer.save()
        messages.success(request, 'farmer updated  successfully')

        return redirect('farmer')


def delete_expense(request, id):
    farmer = Farmer_tbl.objects.get(pk=id)
    farmer.delete()
    messages.success(request, 'farmer removed')
    return redirect('farmer')
