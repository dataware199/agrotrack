from django.apps import AppConfig


class FarmerTblConfig(AppConfig):
    name = 'farmer_tbl'
