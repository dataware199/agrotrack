from django.db import models
from django.utils.timezone import now
# Create your models here.


class Farmer_tbl(models.Model):
  
    name_crop = models.TextField()
    date = models.DateField(default=now)
    first_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    age = models.IntegerField()
    date_birth = models.DateField(default=now)
    identification = models.CharField(max_length=200)
    crop_id = models.CharField(max_length=255)
    region_id = models.CharField(max_length=255)
    
    class Meta:
        ordering: ['+id']

 