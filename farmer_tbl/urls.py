from django.urls import path
from . import views

from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('', views.index, name="farmer"),
    path('add-farmer', views.add_farmer, name="add-farmer"),
   # path('edit-farmer/<int:id>', views.farmer_edit, name="farmer-edit"),
  #  path('farmer-delete/<int:id>', views.delete_farmer, name="farmer-delete"),
    #path('search-farmer', csrf_exempt(views.search_farmer),
     #    name="search_farmer"),
    #path('farmer_category_summary', views.farmer_category_summary,
 #        name="farmer_category_summary"),
  #  path('stats', views.stats_view,
   #      name="stats")
]
