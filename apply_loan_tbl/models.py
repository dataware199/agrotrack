from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
# Create your models here.


class Apply_loan_tbl(models.Model):
    amount_loan_requested = models.TextField()
    total_land_owner_farmer = models.DateField(default=now)
    total_income = models.IntegerField()
    first_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    total_liability = models.CharField(max_length=200)
    age_of_borrower = models.CharField(max_length=200)
    marital_status = models.CharField(max_length=200)
    avaliability_bank_account = models.CharField(max_length=200)
    years_transacting_bank = models.CharField(max_length=200)
    type_loans = models.CharField(max_length=200)
    tenure_farming = models.CharField(max_length=200)
    part_cooperative = models.CharField(max_length=200)
    age_borrower = models.IntegerField()
    date_birth = models.DateField(default=now)
    identification = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    crop_id = models.CharField(max_length=255)
    region_id = models.CharField(max_length=255)
    class Meta:
        ordering: ['-id']
