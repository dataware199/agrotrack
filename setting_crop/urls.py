from django.urls import path, include
from . import views
from django.views.decorators.csrf import csrf_exempt

urlpatterns =[
   
    path('',views.dashboard,name="setting_crop"),
    path('crop',views.index,name="crop"),
    path('search-crop', csrf_exempt(views.search_crop),
         name="search-crop"),
    path('add_setting_crop',views.add_setting_crop, name="add_setting_crop"),
    path('edit-crop/<int:id>', views.edit_setting_crop, name='edit-crop'),
    path('delete-setting/<int:id>', views.delete_setting_crop, name="delete-setting"),
]
