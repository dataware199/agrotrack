from django.shortcuts import render, redirect 
from .models import Setting_crop
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.paginator import Paginator
import json
from django.http import JsonResponse
import datetime
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse


# Create your views here.
def index(request):
    setting_crop = Setting_crop.objects.all()
    paginator = Paginator(setting_crop, 5)
    page_number = request.GET.get('page')
    page_obj = Paginator.get_page(paginator, page_number)
    context = {
        'setting_crop': setting_crop,
        'page_obj': page_obj,     
    }
    return render(request,'setting_crop/index.html',context)
# Create your views here.
def dashboard(request):
    return render(request,'setting_crop/dashboard.html')

def search_crop(request):
    if request.method == 'POST':
        search_str = json.loads(request.body).get('searchText')
        setting_crop = Setting_crop.objects.filter(
            nama_crop__istartswith=search_str, ) | Setting_crop.objects.filter(
            date__istartswith=search_str,) | Setting_crop.objects.filter(
            description__icontains=search_str, )
        data = setting_crop.values()
        return JsonResponse(list(data), safe=False)




def add_setting_crop(request):
    if request.method == 'GET':
        return render(request, 'setting_crop/add_setting_crop.html' )

    if request.method == 'POST':
        name_crop = request.POST['name_crop']

        if not name_crop:
            messages.error(request, 'Crop is required')
            return render(request, 'setting_crop/add_setting_crop).html')
        description = request.POST['description']
       
       

        if not description:
            messages.error(request, 'description is required')
            return render(request, 'expenses/add_expense.html')

        Setting_crop.objects.create(name_crop=name_crop, 
                              description=description)
        messages.success(request, 'Crop saved successfully')

        return redirect('setting_crop')


def delete_setting_crop(request, id):
    setting_crop = Setting_crop.objects.get(pk=id)
    setting_crop.delete()
    messages.success(request, 'Setting Crop removed')
    return redirect('setting_crop')


def edit_setting_crop(request, id):
    setting_crop = Setting_crop.objects.get(pk=id)
    context = {
        'setting_crop': setting_crop,
        'values': setting_crop,
       
    }
    if request.method == 'GET':
       return render(request, 'setting_crop/edit-crop.html', context)
        
    if request.method == 'POST':
        name_crop = request.POST['name_crop']

        if not name_crop:
            messages.error(request, 'Crop is required')
            return render(request, 'setting_crop/edit-crop.html', context)
            
        description = request.POST['description']    
        if not description:
            messages.error(request, 'description is required')
            return render(request, 'setting_crop/edit-crop.html', context)

      
        setting_crop.name_crop = name_crop
        setting_crop.description = description

        setting_crop.save()
        messages.success(request, 'Crop updated  successfully')

        return redirect('setting_crop')

