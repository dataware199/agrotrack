from django.apps import AppConfig


class SettingCropConfig(AppConfig):
    name = 'setting_crop'
