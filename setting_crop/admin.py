from django.contrib import admin
from .models import Setting_crop
# Register your models here.
class Setting_cropAdmin(admin.ModelAdmin):
    list_display = ('description', 'name_crop', 'date',)
    search_fields = ('description', 'name_crop', 'date',)

    list_per_page = 5


admin.site.register(Setting_crop, Setting_cropAdmin)

