from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
# Create your models here.


class Setting_crop(models.Model):
  
    name_crop = models.TextField()
    date = models.DateField(default=now)
    description = models.TextField()
    class Meta:
        ordering: ['-date']

