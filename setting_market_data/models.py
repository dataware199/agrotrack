from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
# Create your models here.


class Setting_market_data(models.Model):
    symbold_traded = models.TextField()
    date = models.DateField(default=now)
    volume_trade = models.IntegerField()
    crop_id = models.CharField(max_length=200)
    open_price = models.CharField(max_length=200)
    price_farmer = models.CharField(max_length=200)
    date_entry = models.CharField(max_length=200)
    age_of_borrower = models.CharField(max_length=200)
    open_price = models.CharField(max_length=200)
    closing_price = models.CharField(max_length=200)
    class Meta:
        ordering: ['-id']
