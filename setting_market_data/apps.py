from django.apps import AppConfig


class SettingMarketDataConfig(AppConfig):
    name = 'setting_market_data'
