from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
# Create your models here.


class Setting_district(models.Model):
   
    name_district = models.TextField()
    region_id = models.TextField()
    description = models.TextField()
    
    class Meta:
        ordering: ['-id']
