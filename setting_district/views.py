from django.shortcuts import render, redirect
from .models import Setting_district
# Create your views here.
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.paginator import Paginator
import json
from django.http import JsonResponse
import datetime
from django.http import HttpResponse, HttpResponseRedirect





def index(request):
    
    district = Setting_district.objects.all()
    #paginator = Paginator(district, 5)
   #page_number = request.GET.get('page')
    #page_obj = Paginator.get_page()
   
    context = {
        'district': district,
       # 'page_obj': page_obj
       
    }
    return render(request, 'district/index.html', context)
