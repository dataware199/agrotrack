from django.contrib import admin
from .models import Setting_district
# Register your models here.


class Setting_districtAdmin(admin.ModelAdmin):
    list_display = ('name_district', 'region_id','description')
    search_fields = ('description', 'category' )

    list_per_page = 5


admin.site.register(Setting_district, Setting_districtAdmin)

