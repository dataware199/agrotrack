from django.urls import path
from . import views

from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('', views.index, name="district"),
  #  path('add-district', views.add_district, name="add-district"),
  #  path('edit-district/<int:id>', views.district_edit, name="district-edit"),
   # path('district-delete/<int:id>', views.delete_district, name="district-delete"),
   # path('search-district', csrf_exempt(views.search_district),
 #        name="search_district"),
   # path('district_category_summary', views.district_category_summary,
   #      name="district_category_summary"),
   # path('stats', views.stats_view,
     #    name="stats")
]
