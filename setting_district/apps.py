from django.apps import AppConfig


class SettingDistrictConfig(AppConfig):
    name = 'setting_district'
